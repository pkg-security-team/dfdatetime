dfdatetime (20240220-2) unstable; urgency=medium

  * Team upload.

  * d/control:
    - Drop python3-mock from Build-Depends (Closes: #1069914).
    - Bump Standards-Version to 4.7.0.
  * d/watch:
    - Drop unneeded option "dversionmangle".
    - Add option "filenamemangle".
  * d/copyright: Update my email address to the d.o one.
  * d/salsa-ci.yml: Add variable SALSA_CI_DISABLE_BUILD_PACKAGE_ANY.

 -- Sven Geuer <sge@debian.org>  Tue, 25 Jun 2024 21:42:49 +0200

dfdatetime (20240220-1) unstable; urgency=medium

  * Team upload.

  [ Sven Geuer ]
  * Update d/watch to recognize new upstream versions again.
  * New upstream version 20240220 (Closes: #1060977).
  * Update d/copyright:
    - Bump upstream's copyright years to 2024.
    - Add missing copyright holders regarding debian/*.
  * Update d/control:
    - Fix lintian issue missing-prerequisite-for-pyproject-backend.
    - Bump Standards-Version to 4.6.2.
    - Apply 'wrap-and-sort -at'.
  * Update d/rules:
    - Drop override_dh_missing, 'dh_missing --fail-missing' is the default now.
    - Replace override_dh_auto_install by execute_after_dh_auto_install.

  [ Samuel Henrique ]
  * d/copyright: Update upstream details

 -- Sven Geuer <debmaint@g-e-u-e-r.de>  Fri, 08 Mar 2024 22:19:02 +0100

dfdatetime (20210509-1) unstable; urgency=medium

  * Team upload.

  * Update debian/watch wrt. to changed Github link structure.
  * Bump debian/watch to version 4.
  * Import new upstream release 20210509.
  * Declare compliance with Debian Policy 4.6.0. (No changes needed.)
  * Add "/*.egg-info/" to debian/clean to be able to build twice in a row.

 -- Axel Beckert <abe@debian.org>  Mon, 25 Oct 2021 20:41:31 +0200

dfdatetime (20200824-1) unstable; urgency=medium

  [ Samuel Henrique ]
  * Team upload.
  * Configure git-buildpackage.

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.5.0, no changes needed.

  [ Francisco Vilmar Cardoso Ruviaro ]
  * New upstream version 20200824.
  * Bump DH level to 13.
  * debian/control: add 'Rules-Requires-Root: no'.
  * debian/copyright: update upstream and packaging copyright years.
  * debian/rules: add 'override_dh_auto_install' for remove extra license file.
  * debian/upstream/metadata: add fields: Repository and Repository-Browse.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Thu, 15 Oct 2020 00:56:15 +0000

dfdatetime (20190116-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop python2 support; Closes: #942957, #936387

 -- Sandro Tosi <morph@debian.org>  Mon, 30 Dec 2019 13:04:58 -0500

dfdatetime (20190116-1) unstable; urgency=medium

  * New upstream version 20190116

 -- Hilko Bengen <bengen@debian.org>  Tue, 22 Jan 2019 12:58:01 +0100

dfdatetime (20181025-1) unstable; urgency=medium

  * New upstream version 20181025
  * Bump Debhelper compat level
  * Replace dh_install --fail-missing
  * Bump Standards-Version
  * Add not-installed (dh_missing bug?)

 -- Hilko Bengen <bengen@debian.org>  Wed, 26 Dec 2018 18:16:24 +0100

dfdatetime (20180704-1) unstable; urgency=medium

  * New upstream version 20180704
  * Bump Standards-Version

 -- Hilko Bengen <bengen@debian.org>  Fri, 03 Aug 2018 17:30:17 +0200

dfdatetime (20180324-1) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org

  [ Hilko Bengen ]
  * New upstream version 20180324

 -- Hilko Bengen <bengen@debian.org>  Wed, 28 Mar 2018 15:52:58 +0200

dfdatetime (20180110-1) unstable; urgency=medium

  * New upstream version 20180110

 -- Hilko Bengen <bengen@debian.org>  Mon, 15 Jan 2018 13:01:33 +0100

dfdatetime (20171228-1) unstable; urgency=medium

  * New upstream version 20171228
  * Bump Standards-Version

 -- Hilko Bengen <bengen@debian.org>  Sun, 31 Dec 2017 15:09:58 +0100

dfdatetime (20171129-1) unstable; urgency=medium

  * New upstream version 20171129

 -- Hilko Bengen <bengen@debian.org>  Mon, 11 Dec 2017 11:21:42 +0100

dfdatetime (20171109-1) unstable; urgency=medium

  * New upstream version 20171109

 -- Hilko Bengen <bengen@debian.org>  Fri, 10 Nov 2017 09:27:34 +0100

dfdatetime (20170719-1) unstable; urgency=medium

  * New upstream version 20170719

 -- Hilko Bengen <bengen@debian.org>  Thu, 20 Jul 2017 07:15:05 +0200

dfdatetime (20170704-1) unstable; urgency=medium

  * New upstream version 20170704
  * Bump Standards-Version, Debhelper compat level

 -- Hilko Bengen <bengen@debian.org>  Wed, 05 Jul 2017 09:06:18 +0200

dfdatetime (20170605-1) unstable; urgency=medium

  * New upstream version 20170605

 -- Hilko Bengen <bengen@debian.org>  Tue, 06 Jun 2017 10:29:17 +0200

dfdatetime (20170529-1) unstable; urgency=medium

  * New upstream version 20170529
  * Update build-dependencies

 -- Hilko Bengen <bengen@debian.org>  Sat, 03 Jun 2017 11:48:26 +0200

dfdatetime (20170103-1) unstable; urgency=medium

  * New upstream version 20170103

 -- Hilko Bengen <bengen@debian.org>  Sat, 07 Jan 2017 20:28:25 +0100

dfdatetime (20161227-1) unstable; urgency=medium

  * New upstream version 20161227

 -- Hilko Bengen <bengen@debian.org>  Mon, 02 Jan 2017 15:45:50 +0100

dfdatetime (20161101-1) unstable; urgency=medium

  * New upstream version 20161101

 -- Hilko Bengen <bengen@debian.org>  Sat, 05 Nov 2016 00:24:52 +0100

dfdatetime (20161017-1) unstable; urgency=medium

  * New upstream version 20161017

 -- Hilko Bengen <bengen@debian.org>  Tue, 18 Oct 2016 01:23:19 +0200

dfdatetime (20160323-1) unstable; urgency=medium

  * Initial release (Closes: #823001)

 -- Hilko Bengen <bengen@debian.org>  Fri, 29 Apr 2016 23:40:03 +0200
